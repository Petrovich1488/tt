"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Expression_1 = require("./Expression");
const Implication_1 = require("./Implication");
const utils_1 = require("./utils");
const TypeVariable_1 = require("./TypeVariable");
const Def_1 = require("./Def");
const Conjunction_1 = require("./Conjunction");
const Equal_1 = require("./Equal");
const Existential_1 = require("./Existential");
class Abstraction extends Expression_1.Expression {
    constructor(expression1, expression2) {
        super(expression1, expression2);
    }
    getType() {
        return 'l';
    }
    toString() {
        return `\\${this.expression1.toString()}.${this.expression2.toString()}`;
    }
    uniqueVariable(mapping, free, result) {
        let s = this.expression1.toString();
        s = `abs${s}`;
        while (Array.from(mapping.values()).includes(s)) {
            s += '0';
        }
        result.set(this.expression1.toExpression(), s);
        let ss = mapping.get(s);
        mapping.set(this.expression1.toString(), s);
        this.expression2.uniqueVariable(mapping, free, result);
        if (ss) {
            mapping.set(s, ss);
        }
        else {
            mapping.delete(s);
        }
    }
    check(variable, bound, free) {
        let b = !bound.has(this.expression1.toString());
        bound.add(this.expression1.toString());
        let k = this.expression2.check(variable, bound, free);
        if (b) {
            bound.delete(this.expression1.toString());
        }
        return k;
    }
    free(bound, free) {
        let was = !bound.has(this.expression1.toString());
        bound.add(this.expression1.toString());
        this.expression2.free(bound, free);
        if (was) {
            bound.delete(this.expression1.toString());
        }
        return free;
    }
    makeSystem(types, system, mapping, intermediate) {
        return new Implication_1.Implication(this.expression1.makeSystem(types, system, mapping, intermediate), this.expression2.makeSystem(types, system, mapping, intermediate));
    }
    W(context, mapping, intermediate) {
        let tempType = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        context.set(mapping.get(this.expression1.toExpression()), tempType);
        let pp2 = this.expression2.W(context, mapping, intermediate);
        for (let pair of pp2.getKey()) {
            tempType = tempType.replace(pair.getKey(), pair.getValue());
        }
        context.delete(mapping.get(this.expression1.toExpression()));
        return new utils_1.Pair(pp2.getKey(), new Implication_1.Implication(tempType, pp2.getValue()));
    }
    buildRestriction(type, intermediate) {
        let tempType1 = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        let tempType2 = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        let expr2Restrictions = this.expression2.buildRestriction(tempType2, intermediate);
        let def = new Def_1.default(this.expression1, tempType1, new Conjunction_1.default(expr2Restrictions, new Equal_1.default(new Implication_1.Implication(tempType1, tempType2), type)));
        return new Existential_1.default(tempType1, new Existential_1.default(tempType2, def));
    }
}
exports.Abstraction = Abstraction;
//# sourceMappingURL=Abstraction.js.map