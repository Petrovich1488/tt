import {Expression} from "./Expression";
import {ExpressionAdapter} from "./ExpressionAdapter";
import {Implication} from "./Implication";
import {Type} from "./Type";
import {Pair} from "./utils";
import {TypeVariable} from "./TypeVariable";
import Def from "./Def";
import Conjunction from "./Conjunction";
import Equal from "./Equal";
import Existential from "./Existential";

export class Abstraction extends Expression {
    constructor(expression1: ExpressionAdapter, expression2: ExpressionAdapter) {
        super(expression1, expression2);
    }

    public getType(): string {
        return 'l';
    }

    public toString(): string {
        return `\\${this.expression1.toString()}.${this.expression2.toString()}`;
    }

    public uniqueVariable(mapping: Map<string, string>, free: Map<string, string>, result: Map<Expression, string>) {
        let s = this.expression1.toString();
        s = `abs${s}`;
        while (Array.from(mapping.values()).includes(s)) {
            s += '0';
        }

        result.set(this.expression1.toExpression(), s);
        let ss = mapping.get(s);
        mapping.set(this.expression1.toString(), s);
        this.expression2.uniqueVariable(mapping, free, result);

        if (ss) {
            mapping.set(s, ss);
        } else {
            mapping.delete(s);
        }
    }

    public check(variable: Expression, bound: Set<string>, free: Set<string>): boolean {
        let b = !bound.has(this.expression1.toString());
        bound.add(this.expression1.toString());
        let k = this.expression2.check(variable, bound, free);
        if (b) {
            bound.delete(this.expression1.toString());
        }

        return k;
    }

    free(bound: Set<string>, free: Set<string>): Set<string> {
        let was = !bound.has(this.expression1.toString());
        bound.add(this.expression1.toString());
        this.expression2.free(bound, free);
        if (was) {
            bound.delete(this.expression1.toString());
        }

        return free;
    }

    makeSystem(types: Map<string, Type>, system: Array<Pair<Type, Type>>, mapping: Map<Expression, string>, intermediate: { n: number }): Type {
        return new Implication(this.expression1.makeSystem(types, system, mapping, intermediate), this.expression2.makeSystem(types, system, mapping, intermediate));
    }

    W(context: Map<string, Type>, mapping: Map<Expression, string>, intermediate: { n: number }): Pair<Array<Pair<Type, Type>>, Type> {
        let tempType:Type = new TypeVariable("a" + ++intermediate.n);
        context.set(mapping.get(this.expression1.toExpression()), tempType);
        let pp2 = this.expression2.W(context, mapping, intermediate);
        for (let pair of pp2.getKey()) {
            tempType = tempType.replace(pair.getKey(), pair.getValue());
        }

        context.delete(mapping.get(this.expression1.toExpression()));
        return new Pair(pp2.getKey(), new Implication(tempType, pp2.getValue()));
    }

    buildRestriction(type: Type, intermediate: { n: number }): Type {
        let tempType1 = new TypeVariable("a" + ++intermediate.n);
        let tempType2 = new TypeVariable("a" + ++intermediate.n);
        let expr2Restrictions = this.expression2.buildRestriction(tempType2, intermediate);

        let def = new Def(this.expression1, tempType1, new Conjunction(expr2Restrictions, new Equal(new Implication(tempType1, tempType2), type)));

        return new Existential(tempType1, new Existential(tempType2, def));
    }
}
