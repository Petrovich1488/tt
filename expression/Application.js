"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Expression_1 = require("./Expression");
const TypeVariable_1 = require("./TypeVariable");
const utils_1 = require("./utils");
const Implication_1 = require("./Implication");
const Existential_1 = require("./Existential");
const Conjunction_1 = require("./Conjunction");
class Application extends Expression_1.Expression {
    constructor(expression1, expression2) {
        super(expression1, expression2);
    }
    getType() {
        return 'a';
    }
    toString() {
        let s = "";
        if (this.expression1.getType() == 'l') {
            s += "(" + this.expression1.toString() + ")";
        }
        else {
            s += this.expression1.toString();
        }
        s += " ";
        if (this.expression2.getType() != 'v') {
            s += "(" + this.expression2.toString() + ")";
        }
        else {
            s += this.expression2.toString();
        }
        return s;
    }
    uniqueVariable(mapping, free, result) {
        this.expression1.uniqueVariable(mapping, free, result);
        this.expression2.uniqueVariable(mapping, free, result);
    }
    check(variable, bound, free) {
        return this.expression1.check(variable, bound, free) && this.expression2.check(variable, bound, free);
    }
    free(bound, free) {
        this.expression1.free(bound, free);
        this.expression2.free(bound, free);
        return free;
    }
    makeSystem(types, system, mapping, intermediate) {
        let tVar = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        system.push(new utils_1.Pair(this.expression1.makeSystem(types, system, mapping, intermediate), new Implication_1.Implication(this.expression2.makeSystem(types, system, mapping, intermediate), tVar)));
        return tVar;
    }
    W(context, mapping, intermediate) {
        let pp = this.expression1.W(context, mapping, intermediate);
        let newContext = new Map();
        for (let [key, value] of context.entries()) {
            let tt = value;
            for (let p of pp.getKey()) {
                tt = tt.replaceBound(p.getKey(), p.getValue(), new Set());
            }
            newContext.set(key, tt);
        }
        let pp2 = this.expression2.W(newContext, mapping, intermediate);
        let tt = pp.getValue();
        for (let p of pp2.getKey()) {
            tt = tt.replace(p.getKey(), p.getValue());
        }
        let tempType = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        let system = [new utils_1.Pair(tt, new Implication_1.Implication(pp2.getValue(), tempType))];
        if (utils_1.solve(system)) {
            system = system.concat(pp.getKey());
            system = system.concat(pp2.getKey());
            if (!utils_1.solve(system)) {
                throw new Error('Nah');
            }
            for (let ppp of system) {
                tempType = tempType.replace(ppp.getKey(), ppp.getValue());
            }
            return new utils_1.Pair(system, tempType);
        }
        else {
            throw new Error('Naah');
        }
    }
    buildRestriction(type, intermediate) {
        let tempType = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        return new Existential_1.default(tempType, new Conjunction_1.default(this.expression1.buildRestriction(new Implication_1.Implication(tempType, type), intermediate), this.expression2.buildRestriction(tempType, intermediate)));
    }
}
exports.Application = Application;
//# sourceMappingURL=Application.js.map