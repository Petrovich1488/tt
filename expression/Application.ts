import {Expression} from "./Expression";
import {ExpressionAdapter} from "./ExpressionAdapter";
import {TypeVariable} from "./TypeVariable";
import {Pair, solve} from "./utils";
import {Implication} from "./Implication";
import {Type} from "./Type";
import Existential from "./Existential";
import Conjunction from "./Conjunction";

export class Application extends Expression {
    constructor(expression1: ExpressionAdapter, expression2: ExpressionAdapter) {
        super(expression1, expression2);
    }

    public getType(): string {
        return 'a';
    }

    public toString(): string {
        let  s = "";
        if (this.expression1.getType() == 'l') {
            s += "(" + this.expression1.toString() + ")";
        } else {
            s += this.expression1.toString();
        }
        s += " ";
        if (this.expression2.getType() != 'v') {
            s += "(" + this.expression2.toString() + ")";
        } else {
            s += this.expression2.toString();
        }
        return s;
    }

    public uniqueVariable(mapping: Map<string, string>, free: Map<string, string>, result: Map<Expression, string>) {
        this.expression1.uniqueVariable(mapping, free, result);
        this.expression2.uniqueVariable(mapping, free, result);
    }

    public check(variable: Expression, bound: Set<string>, free: Set<string>): boolean {
        return this.expression1.check(variable, bound, free) && this.expression2.check(variable, bound, free);
    }

    free(bound: Set<string>, free: Set<string>): Set<string> {
        this.expression1.free(bound, free);
        this.expression2.free(bound, free);

        return free;
    }

    makeSystem(types: Map<string, Type>, system: Array<Pair<Type, Type>>, mapping: Map<Expression, string>, intermediate: { n: number }): Type {
        let tVar = new TypeVariable("a" + ++intermediate.n);
        system.push(new Pair(this.expression1.makeSystem(types, system, mapping, intermediate), new Implication(this.expression2.makeSystem(types, system, mapping, intermediate), tVar)));
        return tVar;
    }

    W(context: Map<string, Type>, mapping: Map<Expression, string>, intermediate: { n: number }): Pair<Array<Pair<Type, Type>>, Type> {
        let pp = this.expression1.W(context, mapping, intermediate);
        let newContext:Map<string, Type> = new Map();
        for(let [key, value] of context.entries()) {
            let tt = value;
            for(let p of pp.getKey()) {
                tt = tt.replaceBound(p.getKey(), p.getValue(), new Set());
            }

            newContext.set(key, tt);
        }
        let pp2 = this.expression2.W(newContext, mapping, intermediate);
        let tt = pp.getValue();
        for(let p of pp2.getKey()) {
            tt = tt.replace(p.getKey(), p.getValue());
        }
        let tempType:Type = new TypeVariable("a" + ++intermediate.n);
        let system:Array<Pair<Type, Type>> = [new Pair(tt, new Implication(pp2.getValue(), tempType))];
        if (solve(system)) {
            system = system.concat(pp.getKey());
            system = system.concat(pp2.getKey());
            if (!solve(system)) {
                throw new Error('Nah');
            }
            for (let ppp of system) {
                tempType = tempType.replace(ppp.getKey(), ppp.getValue());
            }

            return new Pair(system, tempType);
        } else {
            throw new Error('Naah');
        }
    }

    buildRestriction(type: Type, intermediate: { n: number }): Type {
        let tempType = new TypeVariable("a" + ++intermediate.n);
        return new Existential(tempType, new Conjunction(this.expression1.buildRestriction(new Implication(tempType, type), intermediate), this.expression2.buildRestriction(tempType, intermediate)))
    }
}