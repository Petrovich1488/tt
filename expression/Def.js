"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Type_1 = require("./Type");
class Def extends Type_1.Type {
    constructor(variable, type1, type2) {
        super();
        this.variable = variable;
        this.type1 = type1;
        this.type2 = type2;
    }
    toString() {
        return `(def ${this.variable.toString()} : ${this.type1.toString()} in ${this.type2.toString()})`;
    }
    contains(variable) {
        return false;
    }
    free(bound, free) {
    }
    getType() {
        return "";
    }
    replace(variable, to) {
        return undefined;
    }
    replaceBound(variable, to, bound) {
        return undefined;
    }
}
exports.default = Def;
//# sourceMappingURL=Def.js.map