"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Type_1 = require("./Type");
class EqualVar extends Type_1.Type {
    constructor(type1, type2) {
        super();
        this.type1 = type1;
        this.type2 = type2;
    }
    toString() {
        return `${this.type1.toString().substring(1)} < ${this.type2.toString()}`;
    }
    contains(variable) {
        return false;
    }
    free(bound, free) {
    }
    getType() {
        return "";
    }
    replace(variable, to) {
        return undefined;
    }
    replaceBound(variable, to, bound) {
        return undefined;
    }
}
exports.default = EqualVar;
//# sourceMappingURL=EqualVar.js.map