import {Type} from "./Type";

export default class EqualVar extends Type {
    constructor(type1: Type, type2: Type) {
        super();
        this.type1 = type1;
        this.type2 = type2;
    }

    public toString(): string {
        return `${this.type1.toString().substring(1)} < ${this.type2.toString()}`;
    }

    contains(variable: Type): boolean {
        return false;
    }

    free(bound: Set<string>, free: Set<Type>): void {
    }

    getType(): string {
        return "";
    }

    replace(variable: Type, to: Type): Type {
        return undefined;
    }

    replaceBound(variable: Type, to: Type, bound: Set<string>): Type {
        return undefined;
    }

}