"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Expression {
    constructor(expression1, expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }
}
exports.Expression = Expression;
//# sourceMappingURL=Expression.js.map