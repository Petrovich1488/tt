import {ExpressionAdapter} from "./ExpressionAdapter";
import {Type} from "./Type";
import {Pair} from "./utils";

export abstract class Expression {
    public expression1: ExpressionAdapter;
    public expression2: ExpressionAdapter;

    protected constructor(expression1: ExpressionAdapter, expression2: ExpressionAdapter) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }

    public abstract getType(): string;

    public abstract toString(): string;

    public abstract uniqueVariable(mapping: Map<string, string>, free: Map<string, string>, result: Map<Expression, string>);

    public abstract check(variable: Expression, bound: Set<string>, free: Set<string>): boolean;

    public abstract free(bound: Set<string>, free: Set<string>): Set<string>;

    public abstract makeSystem(types:Map<string, Type>,
                               system:Array<Pair<Type, Type>>,
                               mapping: Map<Expression, string>,
                               intermediate: {n: number}): Type;

    public abstract W(context: Map<string, Type>, mapping: Map<Expression, string>,
                      intermediate: { n: number }): Pair<Array<Pair<Type, Type>>, Type>;

    public abstract buildRestriction(type: Type, intermediate: { n: number }): Type;
}
