"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ExpressionAdapter {
    constructor(expression) {
        this.expression = expression;
    }
    getType() {
        return this.expression.getType();
    }
    toString() {
        return this.expression.toString();
    }
    toExpression() {
        return this.expression;
    }
    uniqueVariable(mapping, free, result) {
        return this.expression.uniqueVariable(mapping, free, result);
    }
    check(variable, bound, free) {
        return this.expression.check(variable, bound, free);
    }
    free(bound, free) {
        return this.expression.free(bound, free);
    }
    ;
    makeSystem(types, system, mapping, intermediate) {
        return this.expression.makeSystem(types, system, mapping, intermediate);
    }
    W(context, mapping, intermediate) {
        return this.expression.W(context, mapping, intermediate);
    }
    buildRestriction(type, intermediate) {
        return this.expression.buildRestriction(type, intermediate);
    }
    ;
}
exports.ExpressionAdapter = ExpressionAdapter;
//# sourceMappingURL=ExpressionAdapter.js.map