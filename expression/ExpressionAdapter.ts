import {Expression} from "./Expression";
import {Type} from "./Type";
import {Pair} from "./utils";

export class ExpressionAdapter {
    public expression: Expression;

    constructor(expression: Expression) {
        this.expression = expression;
    }

    public getType():string {
        return this.expression.getType();
    }

    public toString():string {
        return this.expression.toString();
    }

    public toExpression(): Expression {
        return this.expression;
    }

    public uniqueVariable(mapping: Map<string, string>, free: Map<string, string>, result: Map<Expression, string>) {
        return this.expression.uniqueVariable(mapping, free, result);
    }

    public check(variable: Expression, bound: Set<string>, free: Set<string>) : boolean {
        return this.expression.check(variable, bound, free);
    }

    public free(bound: Set<string>, free: Set<string>): Set<string> {
        return this.expression.free(bound, free);
    };

    public makeSystem(types:Map<string, Type>, system:Array<Pair<Type, Type>>, mapping: Map<Expression, string>, intermediate: {n: number}): Type {
        return this.expression.makeSystem(types, system, mapping, intermediate);
    }

    W(context: Map<string, Type>, mapping: Map<Expression, string>, intermediate: { n: number }): Pair<Array<Pair<Type, Type>>, Type> {
        return this.expression.W(context, mapping, intermediate);
    }

    public buildRestriction(type: Type, intermediate: { n: number }): Type {
        return this.expression.buildRestriction(type, intermediate);
    };
}