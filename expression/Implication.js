"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Type_1 = require("./Type");
class Implication extends Type_1.Type {
    constructor(type1, type2) {
        super();
        this.type1 = type1;
        this.type2 = type2;
    }
    getType() {
        return "i";
    }
    toString() {
        if (this.type1.getType() === 'i') {
            return `(${this.type1.toString()})->${this.type2.toString()}`;
        }
        else {
            return `${this.type1.toString()}->${this.type2.toString()}`;
        }
    }
    contains(variable) {
        return this.type1.contains(variable) || this.type2.contains(variable);
    }
    replace(variable, to) {
        return new Implication(this.type1.replace(variable, to), this.type2.replace(variable, to));
    }
    replaceBound(variable, to, bound) {
        return new Implication(this.type1.replaceBound(variable, to, bound), this.type2.replaceBound(variable, to, bound));
    }
    free(bound, free) {
        this.type1.free(bound, free);
        this.type2.free(bound, free);
    }
}
exports.Implication = Implication;
//# sourceMappingURL=Implication.js.map