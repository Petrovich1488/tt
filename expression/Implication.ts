import {Type} from "./Type";

export class Implication extends Type {
    constructor(type1: Type, type2: Type) {
        super();
        this.type1 = type1;
        this.type2 = type2;
    }

    getType(): string {
        return "i";
    }

    toString(): string {
        if ( this.type1.getType() === 'i') {
            return `(${this.type1.toString()})->${this.type2.toString()}`
        } else {
            return `${this.type1.toString()}->${this.type2.toString()}`
        }

    }

    contains(variable: Type): boolean {
        return this.type1.contains(variable) || this.type2.contains(variable);
    }

    replace(variable: Type, to: Type): Type {
        return new Implication(this.type1.replace(variable, to), this.type2.replace(variable, to));
    }

    replaceBound(variable: Type, to: Type, bound: Set<string>): Type {
        return new Implication(this.type1.replaceBound(variable, to, bound), this.type2.replaceBound(variable, to, bound));
    }

    free(bound: Set<string>, free: Set<Type>): void {
        this.type1.free(bound, free);
        this.type2.free(bound, free);
    }

}