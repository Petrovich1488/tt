"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Expression_1 = require("./Expression");
const utils_1 = require("./utils");
const Universal_1 = require("./Universal");
const TypeVariable_1 = require("./TypeVariable");
const Sigma_1 = require("./Sigma");
const Existential_1 = require("./Existential");
const Conjunction_1 = require("./Conjunction");
const EqualVar_1 = require("./EqualVar");
const Def_1 = require("./Def");
class Let extends Expression_1.Expression {
    constructor(variable, expression1, expression2) {
        super(expression1, expression2);
        this.variable = variable;
    }
    check(variable, bound, free) {
        return false;
    }
    free(bound, free) {
        return undefined;
    }
    getType() {
        return "let";
    }
    toString() {
        return `let ${this.variable.toString()} = ${this.expression1.toString()} in ${this.expression2.toString()}`;
    }
    makeSystem(types, system, mapping, intermediate) {
        return undefined;
    }
    uniqueVariable(mapping, free, result) {
        let s = this.variable.toString();
        while (Array.from(result.values()).includes(s)) {
            s += "0";
        }
        result.set(this.variable.toExpression(), s);
        let ss = mapping.get(this.variable.toString());
        this.expression1.uniqueVariable(mapping, free, result);
        mapping.set(this.variable.toString(), s);
        this.expression2.uniqueVariable(mapping, free, result);
        if (ss) {
            mapping.set(this.variable.toString(), ss);
        }
        else {
            mapping.delete(this.variable.toString());
        }
    }
    W(context, mapping, intermediate) {
        let pp1 = this.expression1.W(context, mapping, intermediate);
        let tt = pp1.getValue();
        let free = new Set();
        tt.free(new Set(), free);
        let newContext = new Map();
        for (let [key, entry] of context.entries()) {
            let tt2 = entry;
            for (let p of pp1.getKey()) {
                tt2 = tt2.replaceBound(p.getKey(), p.getValue(), new Set());
            }
            newContext.set(key, tt2);
        }
        let unbounded = new Set();
        newContext.forEach(val => val.free(new Set(), unbounded));
        unbounded.forEach(val => free.delete(val));
        for (let t of free) {
            tt = new Universal_1.default(t, tt);
        }
        for (let p of pp1.getKey()) {
            tt = tt.replaceBound(p.getKey(), p.getValue(), new Set());
        }
        newContext.set(mapping.get(this.variable.toExpression()), tt);
        let pp2 = this.expression2.W(newContext, mapping, intermediate);
        let system = [...pp1.getKey(), ...pp2.getKey()];
        if (!utils_1.solve(system)) {
            throw new Error('Naaah');
        }
        return new utils_1.Pair(system, pp2.getValue());
    }
    buildRestriction(type, intermediate) {
        let tempType1 = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        let tempType2 = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
        let variable = this.variable;
        let sigma = new Sigma_1.default(tempType1, this.expression1.buildRestriction(tempType1, intermediate), tempType1);
        let existential = new Existential_1.default(tempType2, new Conjunction_1.default(new EqualVar_1.default(new TypeVariable_1.TypeVariable(variable.toString()), tempType2), this.expression2.buildRestriction(type, intermediate)));
        return new Def_1.default(variable, sigma, existential);
    }
}
exports.Let = Let;
//# sourceMappingURL=Let.js.map