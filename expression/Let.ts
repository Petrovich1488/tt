import {ExpressionAdapter} from "./ExpressionAdapter";
import {Expression} from "./Expression";
import {Type} from "./Type";
import {Pair, solve} from "./utils";
import Universal from "./Universal";
import {TypeVariable} from "./TypeVariable";
import Sigma from "./Sigma";
import Existential from "./Existential";
import Conjunction from "./Conjunction";
import EqualVar from "./EqualVar";
import Def from "./Def";

export class Let extends Expression {
    public variable: ExpressionAdapter;

    constructor(variable: ExpressionAdapter, expression1: ExpressionAdapter, expression2: ExpressionAdapter) {
        super(expression1, expression2);
        this.variable = variable;
    }

    check(variable: Expression, bound: Set<string>, free: Set<string>): boolean {
        return false;
    }

    free(bound: Set<string>, free: Set<string>): Set<string> {
        return undefined;
    }

    getType(): string {
        return "let";
    }

    public toString(): string {
        return `let ${this.variable.toString()} = ${this.expression1.toString()} in ${this.expression2.toString()}`;
    }

    makeSystem(types: Map<string, Type>, system: Array<Pair<Type, Type>>, mapping: Map<Expression, string>, intermediate: { n: number }): Type {
        return undefined;
    }

    uniqueVariable(mapping: Map<string, string>, free: Map<string, string>, result: Map<Expression, string>) {
        let s = this.variable.toString();
        while (Array.from(result.values()).includes(s)) {
            s += "0";
        }

        result.set(this.variable.toExpression(), s);
        let ss = mapping.get(this.variable.toString());
        this.expression1.uniqueVariable(mapping, free, result);
        mapping.set(this.variable.toString(), s);
        this.expression2.uniqueVariable(mapping, free, result);

        if (ss) {
            mapping.set(this.variable.toString(), ss);
        } else {
            mapping.delete(this.variable.toString());
        }
    }

    W(context: Map<string, Type>, mapping: Map<Expression, string>, intermediate: { n: number }): Pair<Array<Pair<Type, Type>>, Type> {
        let pp1 = this.expression1.W(context, mapping, intermediate);
        let tt = pp1.getValue();
        let free = new Set();
        tt.free(new Set(), free);
        let newContext:Map<string, Type> = new Map();

        for (let [key,entry] of context.entries()) {
            let tt2 = entry;
            for(let p of pp1.getKey()) {
                tt2 = tt2.replaceBound(p.getKey(), p.getValue(), new Set());
            }

            newContext.set(key, tt2);
        }

        let unbounded = new Set();
        newContext.forEach(val => val.free(new Set(), unbounded));
        unbounded.forEach(val => free.delete(val));
        for (let t of free) {
            tt = new Universal(t, tt);
        }

        for (let p of pp1.getKey()) {
            tt = tt.replaceBound(p.getKey(), p.getValue(), new Set());
        }

        newContext.set(mapping.get(this.variable.toExpression()), tt);
        let pp2 = this.expression2.W(newContext, mapping, intermediate);
        let system = [...pp1.getKey(), ...pp2.getKey()];
        if (!solve(system)) {
            throw new Error('Naaah');
        }

        return new Pair(system, pp2.getValue());
    }

    buildRestriction(type: Type, intermediate: { n: number }): Type {
        let tempType1 = new TypeVariable("a" + ++intermediate.n);
        let tempType2 = new TypeVariable("a" + ++intermediate.n);
        let variable = this.variable;

        let sigma = new Sigma(tempType1, this.expression1.buildRestriction(tempType1, intermediate), tempType1);
        let existential = new Existential(tempType2, new Conjunction(new EqualVar(new TypeVariable(variable.toString()), tempType2), this.expression2.buildRestriction(type, intermediate)));

        return new Def(variable, sigma, existential);
    }
}