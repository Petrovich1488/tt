"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ExpressionAdapter_1 = require("./ExpressionAdapter");
const Variable_1 = require("./Variable");
const Application_1 = require("./Application");
const Abstraction_1 = require("./Abstraction");
const Let_1 = require("./Let");
class HMParser {
    constructor(input) {
        this.input = input;
        this.pos = 0;
    }
    skip(skipString) {
        while (/\s/.test(this.input[this.pos])) {
            this.pos++;
        }
        if (this.input.startsWith(skipString, this.pos)) {
            this.pos += skipString.length;
        }
    }
    let() {
        this.skip("let");
        let f = this.getVar();
        this.skip("=");
        let inner = this.exp();
        this.skip("in");
        let outer = this.exp();
        return new ExpressionAdapter_1.ExpressionAdapter(new Let_1.Let(f, inner, outer));
    }
    exp() {
        this.skip("");
        if (this.input.startsWith("let", this.pos)) {
            return this.let();
        }
        if (this.input[this.pos] == '\\') {
            return this.lam();
        }
        else {
            let res = this.atom();
            this.skip("");
            while (true) {
                let e = this.atom();
                if (!e)
                    return res;
                res = new ExpressionAdapter_1.ExpressionAdapter(new Application_1.Application(res, e));
                this.skip("");
            }
        }
    }
    lam() {
        this.skip("\\");
        let variable = this.getVar();
        this.skip('.');
        this.skip("");
        let exp = this.exp();
        return new ExpressionAdapter_1.ExpressionAdapter(new Abstraction_1.Abstraction(variable, exp));
    }
    atom() {
        this.skip("");
        if (this.input.startsWith("let", this.pos)) {
            return this.let();
        }
        if (this.input[this.pos] === '(') {
            this.skip("(");
            let e = this.exp();
            this.skip(")");
            return e;
        }
        else if (this.input[this.pos] === '\\') {
            return this.lam();
        }
        else {
            return this.getVar();
        }
    }
    getVar() {
        this.skip("");
        let name = "";
        while (this.input[this.pos] && /[a-z0-9']/.test(this.input[this.pos])) {
            name += this.input[this.pos++];
        }
        if (name == 'in' || name == 'let') {
            this.pos -= name.length;
            name = "";
        }
        if (name.length > 0) {
            return new ExpressionAdapter_1.ExpressionAdapter(new Variable_1.Variable(name));
        }
        else {
            return null;
        }
    }
}
;
exports.parseExpression = (input) => {
    return (new HMParser(input)).exp();
};
//# sourceMappingURL=Parsers.js.map