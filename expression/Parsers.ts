import {ExpressionAdapter} from "./ExpressionAdapter";
import {Variable} from "./Variable";
import {Application} from "./Application";
import {Abstraction} from "./Abstraction";
import {Let} from "./Let";

class HMParser {
    public input: string;
    public pos: number;

    constructor(input: string) {
        this.input =input;
        this.pos = 0;
    }

    private skip(skipString: string): void {
        while (/\s/.test(this.input[this.pos])) {
            this.pos++;
        }

        if (this.input.startsWith(skipString, this.pos)) {
            this.pos += skipString.length;
        }
    }

    public let(): ExpressionAdapter {
        this.skip("let");
        let f = this.getVar();
        this.skip("=");
        let inner = this.exp();
        this.skip("in");
        let outer = this.exp();
        return new ExpressionAdapter(new Let(f, inner, outer));
    }

    public exp(): ExpressionAdapter {
        this.skip("");
        if (this.input.startsWith("let", this.pos)) {
            return this.let()
        }
        if (this.input[this.pos] == '\\') {
            return this.lam()
        } else {
            let res = this.atom();
            this.skip("");
            while (true) {
                let e = this.atom();
                if (!e) return res;
                res = new ExpressionAdapter(new Application(res, e));
                this.skip("")
            }
        }
    }

    public lam(): ExpressionAdapter {
        this.skip("\\");
        let variable = this.getVar();
        this.skip('.');
        this.skip("");
        let exp = this.exp();
        return new ExpressionAdapter(new Abstraction(variable, exp));
    }

    public atom(): ExpressionAdapter {
        this.skip("");
        if (this.input.startsWith("let", this.pos)) {
            return this.let();
        }

        if (this.input[this.pos] === '(') {
            this.skip("(");
            let e = this.exp();
            this.skip(")");
            return e;
        } else if (this.input[this.pos] === '\\') {
            return this.lam();
        } else {
            return this.getVar();
        }
    }

    public getVar(): ExpressionAdapter {
        this.skip("");
        let name = "";
        while (this.input[this.pos] && /[a-z0-9']/.test(this.input[this.pos])) {
            name += this.input[this.pos++];
        }

        if (name == 'in' || name == 'let') {
            this.pos -= name.length;
            name = "";
        }

        if (name.length > 0) {
            return new ExpressionAdapter(new Variable(name));
        } else {
            return null;
        }
    }
};

export const parseExpression = (input: string): ExpressionAdapter  => {
    return (new HMParser(input)).exp();
}