"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Type_1 = require("./Type");
class Sigma extends Type_1.Type {
    constructor(type1, type2, type3) {
        super();
        this.type1 = type1;
        this.type2 = type2;
        this.type3 = type3;
    }
    toString() {
        return `@${this.type1.toString()}[${this.type2.toString()}].${this.type3.toString()}`;
    }
    contains(variable) {
        return false;
    }
    free(bound, free) {
    }
    getType() {
        return "";
    }
    replace(variable, to) {
        return undefined;
    }
    replaceBound(variable, to, bound) {
        return undefined;
    }
}
exports.default = Sigma;
//# sourceMappingURL=Sigma.js.map