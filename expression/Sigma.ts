import {Type} from "./Type";

export default class Sigma extends Type {
    public type3: Type;

    constructor(type1: Type, type2: Type, type3: Type) {
        super();
        this.type1 = type1;
        this.type2 = type2;
        this.type3 = type3;
    }

    public toString(): string {
        return `@${this.type1.toString()}[${this.type2.toString()}].${this.type3.toString()}`;
    }

    contains(variable: Type): boolean {
        return false;
    }

    free(bound: Set<string>, free: Set<Type>): void {
    }

    getType(): string {
        return "";
    }

    replace(variable: Type, to: Type): Type {
        return undefined;
    }

    replaceBound(variable: Type, to: Type, bound: Set<string>): Type {
        return undefined;
    }

}