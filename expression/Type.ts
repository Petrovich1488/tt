export abstract class Type {

    public type1: Type;
    public type2: Type;

    public abstract toString(): string;
    public abstract getType(): string;
    public abstract free(bound: Set<string>, free: Set<Type>): void;
    public abstract contains(variable: Type): boolean;
    public abstract replace(variable: Type, to: Type): Type;
    public abstract replaceBound(variable: Type, to: Type, bound: Set<string>): Type;
}
