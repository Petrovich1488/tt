"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Type_1 = require("./Type");
class TypeVariable extends Type_1.Type {
    constructor(name) {
        super();
        this.name = name;
    }
    getType() {
        return "v";
    }
    toString() {
        return `'${this.name}`;
    }
    contains(variable) {
        return this.toString() === variable.toString();
    }
    replace(variable, to) {
        return this.contains(variable) ? to : this;
    }
    replaceBound(variable, to, bound) {
        return this.contains(variable) && !bound.has(variable.toString()) ? to : this;
    }
    free(bound, free) {
        if (!bound.has(this.toString())) {
            free.add(this);
        }
    }
}
exports.TypeVariable = TypeVariable;
//# sourceMappingURL=TypeVariable.js.map