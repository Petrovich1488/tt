import {Type} from "./Type";

export class TypeVariable extends Type {
    public name: string;
    constructor(name: string) {
        super();
        this.name = name;
    }

    getType(): string {
        return "v";
    }

    toString(): string {
        return `'${this.name}`;
    }

    contains(variable: Type): boolean {
        return this.toString() === variable.toString();
    }

    replace(variable: Type, to: Type): Type {
        return this.contains(variable) ? to : this;
    }

    replaceBound(variable: Type, to: Type, bound: Set<string>): Type {
        return this.contains(variable) && !bound.has(variable.toString()) ? to : this;
    }

    free(bound: Set<string>, free: Set<Type>): void {
        if (!bound.has(this.toString())) {
            free.add(this);
        }
    }
}