"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Type_1 = require("./Type");
class Universal extends Type_1.Type {
    constructor(type1, type2) {
        super();
        this.type1 = type1;
        this.type2 = type2;
    }
    contains(variable) {
        return this.type1.contains(variable) || this.type2.contains(variable);
    }
    getType() {
        return "u";
    }
    replace(variable, to) {
        return this.type2.replace(variable, to);
    }
    replaceBound(variable, to, bound) {
        let had = bound.has(this.type1.toString());
        bound.add(this.type1.toString());
        let tempType = new Universal(this.type1, this.type2.replaceBound(variable, to, bound));
        if (had) {
            bound.delete(this.type1.toString());
        }
        return tempType;
    }
    toString() {
        return `@${this.type1.toString()}.${this.type2.toString()}`;
    }
    free(bound, free) {
        let had = bound.has(this.type1.toString());
        bound.add(this.type1.toString());
        this.type2.free(bound, free);
        if (had) {
            bound.delete(this.type1.toString());
        }
    }
}
exports.default = Universal;
//# sourceMappingURL=Universal.js.map