import {Type} from "./Type";

export default class Universal extends Type {
    constructor(type1: Type, type2: Type) {
        super();
        this.type1 = type1;
        this.type2 = type2;
    }


    contains(variable: Type): boolean {
        return this.type1.contains(variable) || this.type2.contains(variable);
    }

    getType(): string {
        return "u";
    }

    replace(variable: Type, to: Type): Type {
        return this.type2.replace(variable, to);
    }

    replaceBound(variable: Type, to: Type, bound: Set<string>): Type {
        let had = bound.has(this.type1.toString());
        bound.add(this.type1.toString());
        let tempType = new Universal(this.type1, this.type2.replaceBound(variable, to, bound));
        if (had) {
            bound.delete(this.type1.toString());
        }

        return tempType;
    }

    public toString(): string {
        return `@${this.type1.toString()}.${this.type2.toString()}`;
    }

    free(bound: Set<string>, free: Set<Type>): void {
        let had = bound.has(this.type1.toString());
        bound.add(this.type1.toString());
        this.type2.free(bound, free);
        if (had) {
            bound.delete(this.type1.toString());
        }
    }
}