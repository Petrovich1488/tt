"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Expression_1 = require("./Expression");
const utils_1 = require("./utils");
const TypeVariable_1 = require("./TypeVariable");
const EqualVar_1 = require("./EqualVar");
class Variable extends Expression_1.Expression {
    constructor(variable) {
        super(null, null);
        this.name = variable;
    }
    getType() {
        return 'v';
    }
    toString() {
        return this.name;
    }
    uniqueVariable(mapping, free, result) {
        if (mapping.has(this.toString())) {
            result.set(this, mapping.get(this.toString()));
        }
        else {
            if (free.has(this.toString())) {
                result.set(this, free.get(this.toString()));
            }
            else {
                let s = this.toString();
                while (Array.from(result.values()).includes(s)) {
                    s += "0";
                }
                result.set(this, s);
                free.set(this.toString(), s);
            }
        }
    }
    check(variable, bound, free) {
        if (this.toString() === variable.toString() && !bound.has(variable.toString())) {
            return !Array.from(bound).some((val) => free.has(val));
        }
        return true;
    }
    free(bound, free) {
        if (!bound.has(this.toString())) {
            free.add(this.toString());
        }
        return free;
    }
    makeSystem(types, system, mapping, intermediate) {
        if (!types.has(mapping.get(this))) {
            let tVar = new TypeVariable_1.TypeVariable("a" + ++intermediate.n);
            types.set(mapping.get(this), tVar);
        }
        return types.get(mapping.get(this));
    }
    W(context, mapping, intermediate) {
        let type = context.get(mapping.get(this));
        while (type.getType() === 'u') {
            type = type.replace(type.type1, new TypeVariable_1.TypeVariable("a" + ++intermediate.n));
        }
        return new utils_1.Pair([], type);
    }
    buildRestriction(type, intermediate) {
        return new EqualVar_1.default(new TypeVariable_1.TypeVariable(this.toString()), type);
    }
}
exports.Variable = Variable;
//# sourceMappingURL=Variable.js.map