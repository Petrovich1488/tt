import {Expression} from "./Expression";
import {Pair} from "./utils";
import {Type} from "./Type";
import {TypeVariable} from "./TypeVariable";
import EqualVar from "./EqualVar";

export class Variable extends Expression {
    public name: string;

    constructor(variable: string) {
        super(null, null);
        this.name = variable;
    }

    public getType(): string {
        return 'v';
    }

    public toString(): string {
        return this.name;
    }

    public uniqueVariable(mapping: Map<string, string>, free: Map<string, string>, result: Map<Expression, string>) {
        if (mapping.has(this.toString())) {
            result.set(this, mapping.get(this.toString()));
        } else {
            if (free.has(this.toString())) {
                result.set(this, free.get(this.toString()));
            } else {
                let s = this.toString();
                while (Array.from(result.values()).includes(s)) {
                    s += "0";
                }

                result.set(this, s);
                free.set(this.toString(), s);
            }
        }
    }

    public check(variable: Expression, bound: Set<string>, free: Set<string>): boolean {
        if (this.toString() === variable.toString() && !bound.has(variable.toString())) {
            return !Array.from(bound).some((val) => free.has(val));
        }

        return true;
    }

    public free(bound: Set<string>, free: Set<string>): Set<string> {
        if (!bound.has(this.toString())) {
            free.add(this.toString());
        }

        return free;
    }

    makeSystem(types: Map<string, Type>,
               system: Array<Pair<Type, Type>>,
               mapping: Map<Expression, string>,
               intermediate: { n: number }): Type {
        if (!types.has(mapping.get(this))) {
            let tVar = new TypeVariable("a" + ++intermediate.n);
            types.set(mapping.get(this), tVar);
        }

        return types.get(mapping.get(this));
    }

    W(context: Map<string, Type>,
      mapping: Map<Expression, string>,
      intermediate: { n: number }): Pair<Array<Pair<Type, Type>>, Type> {
        let type = context.get(mapping.get(this));
        while (type.getType() === 'u') {
            type = type.replace(type.type1, new TypeVariable("a"+ ++intermediate.n));
        }

        return new Pair([], type);
    }

    buildRestriction(type: Type, intermediate: { n: number }): Type {
        return new EqualVar(new TypeVariable(this.toString()), type);
    }
}