"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ExpressionAdapter_1 = require("./ExpressionAdapter");
const Application_1 = require("./Application");
const Abstraction_1 = require("./Abstraction");
const Variable_1 = require("./Variable");
exports.reduction = (expression) => {
    if (expression.getType() == 'a') {
        if (expression.expression.expression1.getType() == 'l' && expression.expression.expression1.expression.expression2.check(expression.expression.expression1.expression.expression1.expression, new Set(), expression.expression.expression2.free(new Set(), new Set()))) {
            exports.replace(expression.expression.expression1.expression.expression2, expression.expression.expression1.expression.expression1, expression.expression.expression2);
            expression.expression = expression.expression.expression1.expression.expression2.expression;
            return true;
        }
        else {
            return exports.reduction(expression.expression.expression1) || exports.reduction(expression.expression.expression2);
        }
    }
    else {
        return expression.getType() == 'l' && exports.reduction(expression.expression.expression2);
    }
};
exports.replace = (expression, variable, replacing) => {
    if (expression.getType() == 'v' && expression.toString() === variable.toString()) {
        expression.expression = exports.copy(replacing).expression;
    }
    else if (expression.getType() == 'l' && expression.expression.expression1.toString() !== variable.toString()) {
        exports.replace(expression.expression.expression2, variable, replacing);
    }
    else if (expression.getType() == 'a') {
        exports.replace(expression.expression.expression1, variable, replacing);
        exports.replace(expression.expression.expression2, variable, replacing);
    }
};
exports.copy = (expression) => {
    if (expression.getType() == 'v') {
        return new ExpressionAdapter_1.ExpressionAdapter(new Variable_1.Variable(expression.toString()));
    }
    else if (expression.getType() == 'a') {
        return new ExpressionAdapter_1.ExpressionAdapter(new Application_1.Application(exports.copy(expression.expression.expression1), exports.copy(expression.expression.expression2)));
    }
    else {
        return new ExpressionAdapter_1.ExpressionAdapter(new Abstraction_1.Abstraction(exports.copy(expression.expression.expression1), exports.copy(expression.expression.expression2)));
    }
};
class Pair {
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
    getKey() {
        return this.key;
    }
    getValue() {
        return this.value;
    }
    toString() {
        return `${this.getKey().toString()}=${this.getValue().toString()}`;
    }
}
exports.Pair = Pair;
;
exports.solve = (system) => {
    global: while (true) {
        for (let i = 0; i < system.length; i++) {
            let pair = system[i];
            if (pair.getKey().getType() == 'i' && pair.getValue().getType() == 'i') {
                system.push(new Pair(pair.getKey().type1, pair.getValue().type1));
                system.push(new Pair(pair.getKey().type2, pair.getValue().type2));
                system.splice(i--, 1);
            }
            else if (pair.getKey().getType() == 'v' && pair.getValue().getType() == 'v' && pair.getKey().toString() === pair.getValue().toString()) {
                system.splice(i--, 1);
            }
            else if (pair.getKey().getType() == 'i' && pair.getValue().getType() == 'v') {
                system[i] = new Pair(pair.getValue(), pair.getKey());
            }
        }
        for (let i = 0; i < system.length; i++) {
            let pair = system[i];
            if (pair.getValue().contains(pair.getKey())) {
                return false;
            }
            for (let j = 0; j < system.length; j++) {
                if (i != j) {
                    let pair2 = system[j];
                    if (pair2.getKey().contains(pair.getKey()) || pair2.getValue().contains(pair.getKey())) {
                        system.push(new Pair(pair2.getKey().replace(pair.getKey(), pair.getValue()), pair2.getValue().replace(pair.getKey(), pair.getValue())));
                        system.splice(j, 1);
                        continue global;
                    }
                }
            }
        }
        return true;
    }
};
//# sourceMappingURL=utils.js.map