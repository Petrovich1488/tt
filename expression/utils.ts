import {ExpressionAdapter} from "./ExpressionAdapter";
import {Application} from "./Application";
import {Abstraction} from "./Abstraction";
import {Variable} from "./Variable";
import {Expression} from "./Expression";
import {Type} from "./Type";

export const reduction = (expression: ExpressionAdapter) => {
    if (expression.getType() == 'a') {
        if (expression.expression.expression1.getType() == 'l' && expression.expression.expression1.expression.expression2.check(expression.expression.expression1.expression.expression1.expression,
            new Set(), expression.expression.expression2.free(new Set(), new Set()))) {
            replace(expression.expression.expression1.expression.expression2, expression.expression.expression1.expression.expression1, expression.expression.expression2);
            expression.expression = expression.expression.expression1.expression.expression2.expression;
            return true;
        } else {
            return reduction(expression.expression.expression1) || reduction(expression.expression.expression2);
        }
    } else {
        return expression.getType() == 'l' && reduction(expression.expression.expression2);
    }
};

export const replace = (expression: ExpressionAdapter, variable: ExpressionAdapter, replacing: ExpressionAdapter) => {
    if (expression.getType() == 'v' && expression.toString() === variable.toString()) {
        expression.expression = copy(replacing).expression;
    } else if (expression.getType() == 'l' && expression.expression.expression1.toString() !== variable.toString()) {
        replace(expression.expression.expression2, variable, replacing);
    } else if (expression.getType() == 'a') {
        replace(expression.expression.expression1, variable, replacing);
        replace(expression.expression.expression2, variable, replacing);
    }
};

export const copy = (expression: ExpressionAdapter):ExpressionAdapter => {
    if (expression.getType() == 'v') {
        return new ExpressionAdapter(new Variable(expression.toString()));
    } else if (expression.getType() == 'a') {
        return new ExpressionAdapter(new Application(copy(expression.expression.expression1), copy(expression.expression.expression2)));
    } else {
        return new ExpressionAdapter(new Abstraction(copy(expression.expression.expression1), copy(expression.expression.expression2)));
    }
};

export class Pair<T1, T2> {
    private key: T1;
    private value: T2;

    constructor(key: T1, value: T2) {
        this.key = key;
        this.value = value;
    }

    getKey() {
        return this.key;
    }

    getValue() {
        return this.value;
    }

    toString(): string {
        return `${this.getKey().toString()}=${this.getValue().toString()}`;
    }
};

export const solve = (system:Array<Pair<Type, Type>>) => {
    global:
    while (true){
        for(let i = 0; i < system.length; i++) {
            let pair = system[i];
            if (pair.getKey().getType() == 'i' && pair.getValue().getType() == 'i') {
                system.push(new Pair(pair.getKey().type1, pair.getValue().type1));
                system.push(new Pair(pair.getKey().type2, pair.getValue().type2));
                system.splice(i--, 1);
            } else if (pair.getKey().getType() == 'v' && pair.getValue().getType() == 'v' && pair.getKey().toString() === pair.getValue().toString()) {
                system.splice(i--, 1);
            } else if (pair.getKey().getType() == 'i' && pair.getValue().getType() == 'v') {
                system[i] = new Pair(pair.getValue(), pair.getKey());
            }
        }
        for(let i = 0; i < system.length; i++) {
            let pair = system[i];
            if (pair.getValue().contains(pair.getKey())) {
                return false;
            }
            for(let j = 0; j < system.length; j++) {
                if (i != j) {
                    let pair2 = system[j];
                    if (pair2.getKey().contains(pair.getKey()) || pair2.getValue().contains(pair.getKey())) {
                        system.push(new Pair(pair2.getKey().replace(pair.getKey(), pair.getValue()), pair2.getValue().replace(pair.getKey(), pair.getValue())));
                        system.splice(j, 1);
                        continue global;
                    }
                }
            }
        }
        return true;
    }
};