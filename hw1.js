"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path = require("path");
const Parsers_1 = require("./expression/Parsers");
const utils_1 = require("./expression/utils");
const inputFile = path.join(__dirname, 'input', 'task1.in');
const outputFile = path.join(__dirname, 'input', "task1.out");
const map = new Map();
const input = fs_1.readFileSync(inputFile, 'utf8').split('\n')[0].trim();
const output = fs_1.createWriteStream(outputFile, {
    flags: 'w'
});
const reduce = () => {
    let expression = Parsers_1.parseExpression(input);
    let free = new Map();
    expression.uniqueVariable(new Map(), free, map);
    for (const [key, value] of map.entries()) {
        key.name = value;
    }
    while (utils_1.reduction(expression)) {
    }
    utils_1.reduction(expression);
    output.write(expression.toString());
};
reduce();
//# sourceMappingURL=hw1.js.map