import {createWriteStream, readFileSync} from "fs";
import * as path from "path";
import {Expression} from "./expression/Expression";
import {Variable} from "./expression/Variable";
import {ExpressionAdapter} from "./expression/ExpressionAdapter";
import {parseExpression} from "./expression/Parsers";
import {reduction} from "./expression/utils";

const inputFile = path.join(__dirname, 'input', 'task1.in');
const outputFile = path.join(__dirname, 'input', "task1.out");

const map:Map<Expression, string> = new Map();
const input = readFileSync(inputFile, 'utf8').split('\n')[0].trim();
const output = createWriteStream(outputFile , {
    flags: 'w'
});

const reduce = () => {
    let expression:ExpressionAdapter = parseExpression(input);
    let free = new Map();
    expression.uniqueVariable(new Map(), free, map);
    for(const [key, value] of map.entries()) {
        (key as Variable).name = value;
    }
    while (reduction(expression)) {

    }
    reduction(expression);
    output.write(expression.toString());
};

reduce();