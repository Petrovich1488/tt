"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path = require("path");
const Parsers_1 = require("./expression/Parsers");
const utils_1 = require("./expression/utils");
const inputFile = path.join(__dirname, 'input', 'task2.in');
const outputFile = path.join(__dirname, 'input', "task2.out");
const input = fs_1.readFileSync(inputFile, 'utf8').split('\n')[0].trim();
const output = fs_1.createWriteStream(outputFile, {
    flags: 'w'
});
const typeSolve = () => {
    let expression = Parsers_1.parseExpression(input);
    let map = new Map();
    let free = new Map();
    let types = new Map();
    let system = [];
    let intermediate = { n: -1 };
    expression.uniqueVariable(new Map(), free, map);
    let type = expression.makeSystem(types, system, map, intermediate);
    if (utils_1.solve(system)) {
        for (let pair of system) {
            type = type.replace(pair.getKey(), pair.getValue());
        }
        output.write(`${type.toString()}\n`);
        for (let [key, value] of free.entries()) {
            output.write(`${key}:`);
            let ttype = types.get(value);
            for (let pair of system) {
                ttype = ttype.replace(pair.getKey(), pair.getValue());
            }
            output.write(`${ttype.toString()}\n`);
        }
    }
    else {
        output.write('Лямбда-выражение не имеет типа.');
    }
};
typeSolve();
//# sourceMappingURL=hw2.js.map