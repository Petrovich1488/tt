import {createWriteStream, readFileSync} from "fs";
import * as path from "path";
import {Expression} from "./expression/Expression";
import {ExpressionAdapter} from "./expression/ExpressionAdapter";
import {parseExpression} from "./expression/Parsers";
import {Pair, reduction, solve} from "./expression/utils";
import {Type} from "./expression/Type";

const inputFile = path.join(__dirname, 'input', 'task2.in');
const outputFile = path.join(__dirname, 'input', "task2.out");

const input = readFileSync(inputFile, 'utf8').split('\n')[0].trim();
const output = createWriteStream(outputFile , {
    flags: 'w'
});

const typeSolve = () => {
    let expression:ExpressionAdapter = parseExpression(input);
    let map:Map<Expression, string> = new Map();
    let free:Map<string, string> = new Map();
    let types:Map<string, Type> = new Map();
    let system:Array<Pair<Type, Type>> = [];
    let intermediate = {n: -1};

    expression.uniqueVariable(new Map(), free, map);
    let type: Type = expression.makeSystem(types, system, map, intermediate);
    if (solve(system)) {
        for(let pair of system) {
            type = type.replace(pair.getKey(), pair.getValue());
        }
        output.write(`${type.toString()}\n`);

        for(let [key, value] of free.entries()) {
            output.write(`${key}:`);
            let ttype: Type = types.get(value);
            for (let pair of system) {
                ttype = ttype.replace(pair.getKey(), pair.getValue());
            }
            output.write(`${ttype.toString()}\n`);
        }
    } else {
        output.write('Лямбда-выражение не имеет типа.')
    }
};

typeSolve();