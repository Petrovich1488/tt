"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs_1 = require("fs");
const Parsers_1 = require("./expression/Parsers");
const TypeVariable_1 = require("./expression/TypeVariable");
const inputFile = path.join(__dirname, 'input', 'task3.in');
const outputFile = path.join(__dirname, 'input', "task3.out");
const input = fs_1.readFileSync(inputFile, 'utf8').trim();
const output = fs_1.createWriteStream(outputFile, {
    flags: 'w'
});
const typeSolve = () => {
    let expression = Parsers_1.parseExpression(input);
    let map = new Map();
    let free = new Map();
    let context = new Map();
    let intermediate = { n: -1 };
    expression.uniqueVariable(new Map(), free, map);
    for (let value of free.values()) {
        context.set(value, new TypeVariable_1.TypeVariable("a" + ++intermediate.n));
    }
    try {
        let pair = expression.W(context, map, intermediate);
        let tempType = pair.getValue();
        for (let typePair of pair.getKey()) {
            tempType = tempType.replace(typePair.getKey(), typePair.getValue());
        }
        output.write(`${tempType.toString()}\n`);
        for (let [key, value] of free.entries()) {
            output.write(`${key}:`);
            let tt = context.get(value);
            for (let typePair of pair.getKey()) {
                tt = tt.replace(typePair.getKey(), typePair.getValue());
            }
            output.write(`${tt.toString()}\n`);
        }
    }
    catch (e) {
        output.write('Лямбда-выражение не имеет типа.');
    }
};
typeSolve();
//# sourceMappingURL=hw3.js.map