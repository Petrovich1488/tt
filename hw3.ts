import * as path from "path";
import {createWriteStream, readFileSync} from "fs";
import {ExpressionAdapter} from "./expression/ExpressionAdapter";
import {parseExpression} from "./expression/Parsers";
import {Expression} from "./expression/Expression";
import {Type} from "./expression/Type";
import {TypeVariable} from "./expression/TypeVariable";

const inputFile = path.join(__dirname, 'input', 'task3.in');
const outputFile = path.join(__dirname, 'input', "task3.out");

const input = readFileSync(inputFile, 'utf8').trim();
const output = createWriteStream(outputFile , {
    flags: 'w'
});

const typeSolve = () => {
    let expression:ExpressionAdapter = parseExpression(input);
    let map:Map<Expression, string> = new Map();
    let free:Map<string, string> = new Map();
    let context:Map<string, Type> = new Map();
    let intermediate = {n: -1};

    expression.uniqueVariable(new Map(), free, map);
    for (let value of free.values()) {
        context.set(value, new TypeVariable("a" + ++intermediate.n));
    }

    try {
        let pair = expression.W(context, map, intermediate);
        let tempType = pair.getValue();

        for (let typePair of pair.getKey()) {
            tempType = tempType.replace(typePair.getKey(), typePair.getValue());
        }

        output.write(`${tempType.toString()}\n`);
        for (let [key, value] of free.entries()) {
            output.write(`${key}:`);
            let tt = context.get(value);
            for (let typePair of pair.getKey()) {
                tt = tt.replace(typePair.getKey(), typePair.getValue());
            }
            output.write(`${tt.toString()}\n`);
        }
    } catch (e) {
        output.write('Лямбда-выражение не имеет типа.');
    }
};

typeSolve();