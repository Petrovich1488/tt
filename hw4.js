"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs_1 = require("fs");
const Parsers_1 = require("./expression/Parsers");
const TypeVariable_1 = require("./expression/TypeVariable");
const inputFile = path.join(__dirname, 'input', 'task4.in');
const outputFile = path.join(__dirname, 'input', "task4.out");
const input = fs_1.readFileSync(inputFile, 'utf8').trim();
const output = fs_1.createWriteStream(outputFile, {
    flags: 'w'
});
const buildRestriction = () => {
    let expression = Parsers_1.parseExpression(input);
    let rootType = new TypeVariable_1.TypeVariable("t");
    let intermediate = { n: -1 };
    output.write(expression.buildRestriction(rootType, intermediate).toString());
};
buildRestriction();
//# sourceMappingURL=hw4.js.map