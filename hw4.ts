import * as path from "path";
import {createWriteStream, readFileSync} from "fs";
import {ExpressionAdapter} from "./expression/ExpressionAdapter";
import {parseExpression} from "./expression/Parsers";
import {Expression} from "./expression/Expression";
import {Type} from "./expression/Type";
import {TypeVariable} from "./expression/TypeVariable";

const inputFile = path.join(__dirname, 'input', 'task4.in');
const outputFile = path.join(__dirname, 'input', "task4.out");

const input = readFileSync(inputFile, 'utf8').trim();
const output = createWriteStream(outputFile , {
    flags: 'w'
});

const buildRestriction = () => {
    let expression:ExpressionAdapter = parseExpression(input);
    let rootType = new TypeVariable("t");
    let intermediate = {n:-1};
    output.write(expression.buildRestriction(rootType, intermediate).toString());
};

buildRestriction();