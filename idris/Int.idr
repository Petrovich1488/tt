module MyInt
import Setoid

%access public export
%default total

data MyInt : Type where
    MkMyInt: Nat -> Nat -> MyInt

data EquivMyInt : MyInt -> MyInt -> Type where
    ReflMyInt : (eq : a + d = b + c) -> EquivMyInt (MkMyInt a b) (MkMyInt c d)

ProofReflexivityOfEqualMyInt : Reflx EquivMyInt
ProofReflexivityOfEqualMyInt {x = (MkMyInt a b)} = ReflMyInt $ plusCommutative a b

ProofSymmetryOfEqualMyInt : Sym EquivMyInt
ProofSymmetryOfEqualMyInt { x = MkMyInt a b } { s = MkMyInt c d } (ReflMyInt eq) = ReflMyInt $ sym eq2 where
    eq1 : d + a = b + c
    eq1 = rewrite (plusCommutative d a) in eq

    eq2 : d + a = c + b
    eq2 = rewrite (plusCommutative c b) in eq1

ProofTransitiveOfEqualMYInt : Trans EquivMyInt
ProofTransitiveOfEqualMYInt {x = MkMyInt a b} {y = MkMyInt c d} {z = MkMyInt e f} (ReflMyInt eq1) (ReflMyInt eq2) = ReflMyInt step9 where
    step1 : a + d + f = b + c + f
    step1 = plusConstantRight (a + d) (b + c) f eq1

    step2 : a + (d + f) = b + c + f
    step2 = rewrite plusAssociative a d f in step1

    step3 : a + (f + d) = b + c + f
    step3 = rewrite plusCommutative f d in step2

    step4 : a + f + d = b + c + f
    step4 = rewrite sym $ plusAssociative a f d in step3

    step5 : a + f + d = b + (c + f)
    step5 = rewrite plusAssociative b c f in step4

    step6 : a + f + d = b + (d + e)
    step6 = rewrite sym $ eq2 in step5

    step7 : a + f + d = b + (e + d)
    step7 = rewrite plusCommutative e d in step6

    step8 : a + f + d = b + e + d
    step8 = rewrite sym $ plusAssociative b e d in step7

    step9 : a + f = b + e
    step9 = plusRightCancel (a + f) (b + e) d step8

SetoidMyInt : Setoid
SetoidMyInt = MkSetoid MyInt EquivMyInt (EqProof EquivMyInt ProofReflexivityOfEqualMyInt ProofSymmetryOfEqualMyInt ProofTransitiveOfEqualMYInt)


AdditionMyInt : MyInt -> MyInt -> MyInt
AdditionMyInt (MkMyInt a b) (MkMyInt c d) = MkMyInt (a + c) (b + d)

NegateMyInt : MyInt -> MyInt
NegateMyInt (MkMyInt a b) = MkMyInt b a

MultiplyMyIntByNat : MyInt -> Nat -> MyInt
MultiplyMyIntByNat (MkMyInt a b) n = MkMyInt (a * n) (b * n)

MultiplyMyInts : MyInt -> MyInt -> MyInt
MultiplyMyInts (MkMyInt a b) (MkMyInt c d) = MkMyInt (a * c + b * d) (a * d + b * c)