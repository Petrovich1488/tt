module MyRat

import Setoid
import Int

%access public export
%default total

data MyRat = MkMyRat MyInt Nat

data EquivMyRat : MyRat -> MyRat -> Type where
    ReflMyRat : 
        (equiv : EquivMyInt (MultiplyMyIntByNat a (S d)) (MultiplyMyIntByNat c (S b))) 
        -> EquivMyRat (MkMyRat a b) (MkMyRat c d)

ProofReflexivityOfEqualMyRat : Reflx EquivMyRat
ProofReflexivityOfEqualMyRat (MkMyRat a b) = ReflMyRat $ ProofReflexivityOfEqualMyInt (MultiplyMyIntByNat a (S b))

ProofSymmetryOfEqualMyRat : Sym EquivMyRat
ProofSymmetryOfEqualMyRat (MkMyRat a b) (MkMyRat c d) (ReflMyRat aa) 
        = ReflMyRat $ ProofSymmetryOfEqualMyInt (MultiplyMyIntByNat a (S d)) (MultiplyMyIntByNat c (S b)) aa

Mult3AssociativeCommutative : (a : Nat) -> (b : Nat) -> (c : Nat) -> a * b * c = a * c * b
Mult3AssociativeCommutative a b c = rewrite sym $ multAssociative a c b in rewrite multCommutative c b in sym $ multAssociative a b c

MultSuccNeutralBoth : (a,b,c : Nat) -> (S c) * a = (S c) * b -> c * a = c * b
MultSuccNeutralBoth (S a) Z c ok = void $ SIsNotZ step1 where
    step1 : (S c) * (S a) = Z
    step1 = rewrite sym $ multZeroRightZero (S c) in ok
MultSuccNeutralBoth Z (S b) c ok = void $ SIsNotZ $ sym step1 where
    step1 : Z = (S c) * (S b)
    step1 = rewrite sym $ multZeroRightZero (S c) in ok
MultSuccNeutralBoth Z Z c ok = Refl
MultSuccNeutralBoth (S a) (S b) c ok = step3 where
    step1 : (S c) * a = (S c) * b
    step1 = plusLeftCancel (S c) ((S c) * a) ((S c) * b) (
        rewrite sym $ multRightSuccPlus (S c) b in
        rewrite sym $ multRightSuccPlus (S c) a in ok)
    step2 : c + c * a = c + c * b
    step2 = plusConstantLeft (c * a) (c * b) c (MultSuccNeutralBoth a b c step1)

    step3 : c * (S a) = c * (S b)
    step3 = rewrite multRightSuccPlus c a in
            rewrite multRightSuccPlus c b in step2

MultOneRightCancel : (a : Nat) -> (b : Nat) -> (c : Nat) -> (a * S c = b * S c) -> a = b
MultOneRightCancel a b Z ok = rewrite sym $ multOneRightNeutral b in rewrite sym $ multOneRightNeutral a in ok
MultOneRightCancel a b (S c) ok = MultOneRightCancel a b c step2 where
    step1 : (S (S c)) * a = (S (S c)) * b
    step1 = rewrite multCommutative (S (S c)) a in rewrite multCommutative (S (S c)) b in ok

    step2 : a * (S c) = b * (S c)
    step2 = rewrite multCommutative a (S c) in rewrite multCommutative b (S c) in MultSuccNeutralBoth a b (S c) step1

Plus4Neutral : {a : Nat} -> {b : Nat} -> {c : Nat} -> {d : Nat} -> (a = b) -> (c = d) -> (a + c = b + d)
Plus4Neutral ok1 ok2 = rewrite ok2 in rewrite ok1 in Refl

ProofTransititivityOfEqualMyRat : Trans EquivMyRat
ProofTransititivityOfEqualMyRat (MkMyRat (MkMyInt a b) c) (MkMyRat (MkMyInt d e) f) (MkMyRat (MkMyInt g h) i) (ReflMyRat (ReflMyInt eq1)) (ReflMyRat (ReflMyInt eq2)) 
                                = ReflMyRat $ ReflMyInt $ MultOneRightCancel (a * S i + h * S c) (b * S i + g * S c) f step7 where
    step1 : a * S f * S i + e * S i * S c = b * S f * S i + d * S c * S i
    step1 = rewrite Mult3AssociativeCommutative e (S i) (S c) in
          rewrite sym $ multDistributesOverPlusLeft (a * S f) (e * S c) (S i) in
          rewrite sym $ multDistributesOverPlusLeft (b * S f) (d * S c) (S i) in cong { f = (* S i) } eq1

    step2 : d * S c * S i + h * S f * S c = e * S i * S c + g * S f * S c
    step2 = rewrite Mult3AssociativeCommutative d (S c) (S i) in
          rewrite sym $ multDistributesOverPlusLeft (d * S i) (h * S f) (S c) in
          rewrite sym $ multDistributesOverPlusLeft (e * S i) (g * S f) (S c) in cong { f = (* S c) } eq2

    step3 : (a * S f * S i + e * S i * S c) + (d * S c * S i + h * S f * S c) = (b * S f * S i + d * S c * S i) + (e * S i * S c + g * S f * S c)
    step3 = Plus4Neutral step1 step2

    f1 : (a,b,c,d, a', b', c', d' : Nat) -> (a + b) + (c + d) = (a' + b') + (c' + d') -> a + d + (b + c) = a' + d' + (c' + b')
    f1 a b c d a' b' c' d' eq = rewrite plusAssociative (a + d) b c in step7' where
        step1' : (a + b) + (d + c) = (a' + b') + (d' + c')
        step1' = rewrite (plusCommutative d c) in rewrite (plusCommutative d' c') in eq

        step2' : (a + b) + d + c = (a' + b') + d' + c'
        step2' = rewrite sym $ plusAssociative (a + b) d c in rewrite sym $ plusAssociative (a' + b') d' c' in step1'

        step3' : (a + (b + d)) + c = (a' + (b' + d')) + c'
        step3' = rewrite plusAssociative a b d in (rewrite plusAssociative a' b' d' in step2')

        step4' : (a + (d + b)) + c = (a' + (d' + b')) + c'
        step4' = rewrite plusCommutative d b in rewrite plusCommutative d' b' in step3'

        step5' : a + d + b + c = (a' + d') + b' + c'
        step5' = rewrite sym $ plusAssociative a d b in rewrite sym $ plusAssociative a' d' b' in step4'

        step6' : a + d + b + c = a' + d' + (b' + c')
        step6' = rewrite plusAssociative (a' + d') b' c' in step5'

        step7' : a + d + b + c = a' + d' + (c' + b')
        step7' = rewrite plusCommutative c' b' in step6'

    step4 : a * S f * S i + h * S f * S c + (e * S i * S c + d * S c * S i) = b * S f * S i + g * S f * S c + (e * S i * S c + d * S c * S i)
    step4 = f1 (a * S f * S i) (e * S i * S c) (d * S c * S i) (h * S f * S c) (b * S f * S i) (d * S c * S i) (e * S i * S c) (g * S f * S c) step3

    step5 : a * S f * S i + h * S f * S c = b * S f * S i + g * S f * S c
    step5 = plusRightCancel (a * S f * S i + h * S f * S c) (b * S f * S i + g * S f * S c) (e * S i * S c + d * S c * S i) step4

    step6 : a * S i * S f + h * S c * S f = b * S i * S f + g * S c * S f
    step6 = rewrite Mult3AssociativeCommutative a (S i) (S f) in rewrite Mult3AssociativeCommutative h (S c) (S f) in
          rewrite Mult3AssociativeCommutative b (S i) (S f) in rewrite Mult3AssociativeCommutative g (S c) (S f) in step5

    step7 : (a * S i + h * S c) * S f = (b * S i + g * S c) * S f
    step7 = rewrite multDistributesOverPlusLeft (b * S i) (g * S c) (S f) in
          rewrite multDistributesOverPlusLeft (a * S i) (h * S c) (S f) in step6

SetoidMyRat : Setoid
SetoidMyRat = MkSetoid MyRat EquivMyRat (EqProof EquivMyRat ProofReflexivityOfEqualMyRat ProofSymmetryOfEqualMyRat ProofTransititivityOfEqualMyRat)