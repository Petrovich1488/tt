module Setoid
%access public export
%default total

Reflx : {a : Type} -> (rel : a -> a -> Type) -> Type
Reflx {a} rel = (x : a) -> rel x x

Sym : {a : Type} -> (rel : a -> a -> Type) -> Type
Sym {a} rel = (x : a) -> (y : a) -> rel x y -> rel y x

Trans : {a : Type} -> (rel : a -> a -> Type) -> Type
Trans {a} rel = (x : a) -> (y : a) -> (z : a) -> rel x y -> rel y z -> rel x z

data IsEquivalence : {a : Type} -> (rel : a -> a -> Type) -> Type where
    EqProof : {a : Type} -> (rel : a -> a -> Type) -> Reflx {a} rel -> Sym {a} rel -> Trans {a} rel -> IsEquivalence {a} rel

record Setoid where
    constructor MkSetoid
    Carrier : Type
    Equiv : Carrier -> Carrier -> Type
    EquivProof : IsEquivalence Equiv